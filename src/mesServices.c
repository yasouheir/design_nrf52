#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include "mesServices.h"
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>

#define LOG_LEVEL CONFIG_BT_HRS_LOG_LEVEL
#include <zephyr/logging/log.h>

LOG_MODULE_REGISTER(sim);

/* Variables pour stocker l'état des caractéristiques */
static uint8_t hrs_blsc;
static uint16_t version = 0U;
static uint8_t led_state = 0U;
static uint8_t buzzer_state = 0U;
static uint8_t bouton_state = 0U;

/* Variable pour stocker la connexion par défaut */
static struct bt_conn *default_conn;

/* Fonctions de gestion de la connexion Bluetooth */
static void connected(struct bt_conn *conn, uint8_t err)
{
    if (err)
    {
        LOG_ERR("Connection failed (err %u)\n", err);
    }
    else
    {
        LOG_INF("Connected\n");
        if (conn != default_conn)
        {
            LOG_WRN("Not using the default connection.\n");
        }
    }
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
    LOG_WRN("Disconnected (reason %u)\n", reason);
    if (conn == default_conn)
    {
        default_conn = NULL;
    }
}

/* Structure de rappels de connexion Bluetooth */
static struct bt_conn_cb conn_callbacks = {
    .connected = connected,
    .disconnected = disconnected,
};

/* Fonction appelée lors du changement de configuration CCC */
static void hrmc_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value)
{
    ARG_UNUSED(attr);
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notification %s\n", notif_enabled ? "enabled" : "disabled");
}

/* Fonction pour lire la valeur de la caractéristique 'blsc' */
static ssize_t read_blsc(struct bt_conn *conn, const struct bt_gatt_attr *attr,
                         void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &hrs_blsc,
                             sizeof(hrs_blsc));
}

/* Fonction pour lire la version */
static ssize_t read_version(struct bt_conn *conn, const struct bt_gatt_attr *attr,
                            void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &version,
                             sizeof(version));
}

/* ... (ajoutez d'autres fonctions de lecture ou d'écriture si nécessaire) ... */

/* Déclaration du service principal */
BT_GATT_SERVICE_DEFINE(system_service,
                      BT_GATT_PRIMARY_SERVICE(BT_UUID_SYSTEM),
                      /* Caractéristiques du service système */
                      BT_GATT_CHARACTERISTIC(BT_UUID_VERSION, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_version, NULL, &version),
                      BT_GATT_CHARACTERISTIC(BT_UUID_LED, BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, NULL, NULL, &led_state),
                      BT_GATT_CHARACTERISTIC(BT_UUID_BUZZER, BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, NULL, NULL, &buzzer_state),
                      BT_GATT_CHARACTERISTIC(BT_UUID_BOUTON, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, NULL, NULL, &bouton_state),
                      BT_GATT_CHARACTERISTIC(BT_UUID_ADVERTISING, BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, NULL, NULL, NULL),
                      /* Descripteur de caractéristique utilisateur */
                      BT_GATT_CUD("test", BT_GATT_PERM_READ),
                      /* Configuration CCC */
                      BT_GATT_CCC(hrmc_ccc_cfg_changed, HRS_GATT_PERM_DEFAULT));

/* Déclaration du service d'interface */
BT_GATT_SERVICE_DEFINE(interface_service,
                      BT_GATT_PRIMARY_SERVICE(BT_UUID_INTERFACE),
                      /* Caractéristiques du service d'interface */
                      BT_GATT_CHARACTERISTIC(BT_UUID_DEBUG, BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
                                             BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, NULL, NULL, NULL),
                      BT_GATT_CHARACTERISTIC(BT_UUID_BAS, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                                             BT_GATT_PERM_READ, NULL, NULL, NULL),
                      /* Descripteur de caractéristique utilisateur */
                      BT_GATT_CUD("test", BT_GATT_PERM_READ),
                      /* Configuration CCC */
                      BT_GATT_CCC(hrmc_ccc_cfg_changed, HRS_GATT_PERM_DEFAULT));

/* Fonction pour notifier l'état de la LED */
static void notify_led_state(void)
{
    int rc;
    static uint8_t data[1];
    data[0] = led_state;

    rc = bt_gatt_notify(NULL, &system_service.attrs[3], &data, sizeof(data));
    if (rc == -ENOTCONN)
    {
        LOG_INF("Notification: LED state not sent (no connection)\n");
    }
    else if (rc)
    {
        LOG_ERR("Failed to send LED state notification, error code: %d\n", rc);
    }
}

/* Fonction pour écrire l'état de la LED */
static ssize_t write_led_state(struct bt_conn *conn, const struct bt_gatt_attr *attr,
                               const void *buf, uint16_t len, uint16_t offset, uint8_t flags)
{
    if (len != sizeof(uint8_t))
    {
        return -EINVAL;
    }

    led_state = *((uint8_t *)buf);
    LOG_INF("LED state set to %u\n", led_state);

    /* Notifier la nouvelle valeur de la LED aux abonnés */
    notify_led_state();

    return len;
}



/* Fonction d'initialisation */
static int sim_init(void)
{
    int err;

    /* Initialisation de la variable hrs_blsc */
    hrs_blsc = 0x01;

    /* Enregistrement des rappels de connexion Bluetooth */
    bt_conn_cb_register(&conn_callbacks);

    /* Enregistrement du service système */
    err = bt_gatt_service_register(&system_service);
    if (err)
    {
        LOG_ERR("Failed to register system service, error code: %d\n", err);
        return err;
    }

    /* Enregistrement du service d'interface */
    err = bt_gatt_service_register(&interface_service);
    if (err)
    {
        LOG_ERR("Failed to register interface service, error code: %d\n", err);
        return err;
    }

  

    return 0;
}

SYS_INIT(sim_init, APPLICATION, CONFIG_APPLICATION_INIT_PRIORITY);
