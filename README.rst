# Keyfinder App

## Introduction

La Keyfinder App est une application permettant de communiquer avec une carte embarquée utilisant la technologie Bluetooth Low Energy (BLE). L'application offre une interface conviviale pour interagir avec différents services sur la carte, tels que le contrôle d'une LED, la gestion d'un buzzer, et la surveillance d'un bouton.

## Fonctionnalités

- **Système Service**
  - `Version` : Permet de lire la version du système.
  - `LED` : Permet de contrôler l'état de la LED (allumer/éteindre).
  - `Buzzer` : Permet de contrôler l'état du buzzer (activer/désactiver).
  - `Bouton` : Permet de recevoir des notifications lorsque le bouton est pressé.
  - `Advertising` : Permet de contrôler la fonction d'advertising.

- **Interface Service**
  - `Debug` : Permet de lire et écrire des données de débogage.
  - `Battery Level` : Permet de surveiller le niveau de la batterie.

## Prérequis

- Un appareil compatible Bluetooth Low Energy (BLE).
- La carte embarquée avec le firmware approprié.
- [nRF Connect](https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Connect-for-desktop) ou une autre application compatible BLE.

## Utilisation

1. **Connexion à la carte**
   - Activez le Bluetooth sur votre appareil.
   - Lancez l'application nRF Connect.
   - Recherchez et connectez-vous à la carte avec le nom "Keyfinder".

2. **Exploration des Services**
   - Dans l'application nRF Connect, explorez les services disponibles sous la carte.
   - Vous devriez voir deux services principaux : `System Service` et `Interface Service`.

3. **Interaction avec le Système Service**
   - Dans le service `System Service`, vous pouvez explorer et interagir avec les différentes caractéristiques.
   - Utilisez la caractéristique `LED` pour contrôler l'état de la LED.
   - Utilisez la caractéristique `Buzzer` pour activer ou désactiver le buzzer.
   - La caractéristique `Bouton` vous permettra de recevoir des notifications lorsque le bouton est pressé.

4. **Interaction avec l'Interface Service**
   - Dans le service `Interface Service`, utilisez la caractéristique `Debug` pour lire et écrire des données de débogage.
   - La caractéristique `Battery Level` vous permet de surveiller le niveau de la batterie.

## Interprétation des Résultats

- **LED State**
  - La caractéristique `LED` accepte des valeurs booléennes (0 pour éteindre, 1 pour allumer).
  - L'état actuel de la LED est notifié dans la caractéristique `LED` à chaque changement.

- **Buzzer State**
  - La caractéristique `Buzzer` accepte des valeurs booléennes (0 pour désactiver, 1 pour activer).
  - L'état actuel du buzzer est notifié dans la caractéristique `Buzzer` à chaque changement.

- **Button Press Notification**
  - Chaque fois que le bouton est pressé, une notification est reçue dans la caractéristique `Bouton`.

- **Debugging**
  - Utilisez la caractéristique `Debug` pour lire et écrire des données de débogage selon vos besoins.

- **Battery Level**
  - La caractéristique `Battery Level` fournit le niveau actuel de la batterie.

## Notes

- Assurez-vous que la carte est alimentée et à portée.
- Les caractéristiques notifiant les changements informeront automatiquement l'application lors de modifications.
- Consultez la documentation de la carte pour plus de détails sur les valeurs acceptées et les comportements spécifiques.

---
